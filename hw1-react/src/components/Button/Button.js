import { Component } from "react";

class Button extends Component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <button type="button" className="btn btn-primary" style={{backgroundColor: this.props.backgroundColor}} onClick={this.props.click}>{this.props.name}</button>
        )
    }
}
export default Button