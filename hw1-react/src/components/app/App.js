import { Component } from "react";
import "./App.scss";
import Modal from "../Modal/Modal";
import Button from "../Button/Button";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal1: false,
      modal2: false,
    };
  }

  closeModal = () => {
    this.setState(() => ({
      modal1: false,
      modal2: false,
    }));
  };
  render() {
    return (
      <div className="App">
        <Button
          name="First Button"
          backgroundColor="orange"
          click={() => {
            this.setState(() => ({ modal1: true }));
          }}
        />
        <Button
          name="Second Button"
          backgroundColor="green"
          click={() => {
            this.setState(() => ({ modal2: true }));
          }}
        />

        {this.state.modal1 && (
          <Modal
            isClose={this.closeModal}
            text="It's a text from a first modal"
            header="im a title"
            closeButton={false}
            actions={
              <>
                <Button name="Ok" click={this.closeModal} />
                <Button name="Cancel" click={this.closeModal} />
              </>
            }
          />
        )}

        {this.state.modal2 && (
          <Modal
            isClose={this.closeModal}
            text="It's a text from a second modal"
            header="im a title 2"
            closeButton={false}
            actions={
              <>
                <Button name="Ok" click={this.closeModal} />
                <Button name="Cancel" click={this.closeModal} />
              </>
            }
          />
        )}
      </div>
    );
  }
}

export default App;
