import React from "react";
import { Component } from "react";
class Modal extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { closeButton, text, header, isClose, actions } = this.props;
    
    return (
      <div className="dark" onClick={isClose}>
        <div class="modal" tabindex="-1" >
          <div class="modal-dialog" onClick={event => event.stopPropagation()}>
            <div class="modal-content">
              <div class="modal-header">
                <h5  class="modal-title">{header}</h5>
                {closeButton && (
                  <button
                    onClick={isClose}
                    type="button"
                    class="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                  ></button>
                )}
              </div>
              <div class="modal-body">
                <p>{text}</p>
              </div>
              <div class="modal-footer">{actions}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
