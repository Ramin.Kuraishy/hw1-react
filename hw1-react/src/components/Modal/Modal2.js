import React from "react";

export default function Modal2({open1, children, onClose1}) {
    
    if(!open1 || onClose1){
        return null
    }
    return  (
        <div>
            {children}
        </div>
        )
    
    }

